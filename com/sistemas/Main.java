package com.sistemas;


public class Main {

    public static void main(String[] args) {
        GameBoardImpl gb = new GameBoardImpl(3);

        gb.riscar(0,0); //Ponto Invalido (é o indice da marcação)

        //Fechar Quadrado canto esquerdo superior:
        gb.riscar(0,1);
        gb.riscar(1,2);
        gb.riscar(2,1);
        gb.riscar(1,0);

        //Fechar Quadrado canto esquerdo inferior:
        gb.riscar(5,0);
        gb.riscar(5,2);
        gb.riscar(4,1);
        gb.riscar(6,1);

        //Fecha no esquerdo central
        gb.riscar(3,0);
        //gb.riscar(3,2);

        gb.mostrarMatriz();



    }

}