package com.sistemas;

import com.sun.javaws.exceptions.InvalidArgumentException;

/**
 * Created by matheusprusch on 5/12/15.
 */
public class GameBoardImpl implements GameBoard {

    String[][] grid;
    static int gridSize;

    public GameBoardImpl(int s){
        grid = criarMatriz(s);
        gridSize = s;
    }


    private String[][] criarMatriz(int s) {

        int gridSize = s;
        grid = new String[2*gridSize+1][2*gridSize+1];

        for (int i = 0; i<grid.length; i++){
            for (int j=0; j<grid[i].length; j++){
                if(i%2==0){
                    if(j%2==0){
                        grid[i][j]="•";
                    } else {
                        grid[i][j]="   ";
                    }
                } else{
                    if(j%2==0){
                        grid[i][j]=" ";
                    } else {
                        grid[i][j]="   ";
                    }
                }
            }
        }
        return grid;
    }


    public void riscar(int x, int y) {

        try {
            if(x%2!=0 && y%2==0){
                if(grid[x][y] == "|"){
                    throw new InvalidArgumentException(new String[]{"Impossível utilizar estes valor"});
                }
                grid[x][y] = "|";
            }
            else if(x%2==0 && y%2!=0) {
                if(grid[x][y] == "---"){
                    throw new InvalidArgumentException(new String[]{"Impossível utilizar estes valor"});
                }
                grid[x][y] = "---";
            } else {
                throw new InvalidArgumentException(new String[]{"Impossível utilizar estes valor"});
            }

        } catch (Exception e){
            System.out.println("Impossível utilizar estes valores: ("+x+","+y+"). \nPonto já utilizado ou inválido.\n");
        }


        if (verificaFechaQuadrado(x,y, 'M')){
        //    System.out.println("Parabens!");
        }

    }


    public String mostrarMatriz() {
        StringBuilder b = new StringBuilder();

        //Print X-Axis Guide (Header)
        String header = "    ";
        for (int i=0; i<=grid.length;i++){
            if (i<grid.length){
                header += i+" ";
            }
            if(i==grid.length){
                b.append(header);
                b.append("\n");
            }
        }
        //print matrix
        for (int l=0; l<grid.length; l++){
            //Para cada linha:
            b.append(l+"|  ");

            for (int c=0; c<grid[l].length; c++){
                //Para cada Coluna
                b.append(grid[l][c]);

            }
            b.append("\n");


        }
        System.out.println(b.toString());
        return null;
    }


    private boolean verificaFechaQuadrado(int linha, int coluna, char j) {
        try {

            if (linha%2==0) {

                if(linha<=grid.length-2){
                    //Se o jogador riscar na horizontal, verifica se esta completando quadrado abaixo:
                    if (grid[linha + 2][coluna] == "---" && grid[linha + 1][coluna - 1] == "|" && grid[linha + 1][coluna + 1] == "|") {
                        fechaQuadrado(linha+1,coluna,j);
                        return true;
                    }
                }

                if(linha>=2){
                    //Se o jogador riscar na horizontal, verifica se esta completando quadrado acima:
                    if (grid[linha - 2][coluna] == "---" && grid[linha - 1][coluna - 1] == "|" && grid[linha - 1][coluna + 1] == "|") {
                        fechaQuadrado(linha-1,coluna,j);
                        return true;
                    }
                }
            } else {

                if(coluna>=2){
                    //Se o jogador riscar na vertical, verifica se esta completando quadrado à esquerda:
                    if(grid[linha-1][coluna-1]=="---" && grid[linha+1][coluna-1]=="---" && grid[linha][coluna-2]=="|"){
                        fechaQuadrado(linha,coluna-1,j);
                        return true;
                    }
                }

                if(coluna<=grid[linha].length-2){
                    //Se o jogador riscar na vertical, verifica se esta completando quadrado à direita:
                    if(grid[linha-1][coluna+1]=="---" && grid[linha+1][coluna+1]=="---" && grid[linha][coluna+2]=="|"){
                        fechaQuadrado(linha,coluna+1,j);
                        return true;
                    }
                }
            }

        } catch (Exception e){
            System.out.println("Exception?");
            return false;
        }

        return false;
    }

    private void fechaQuadrado(int linha, int coluna, char j){
        grid[linha][coluna] = " "+String.valueOf(j)+" ";
    }


    public void fechaQuadrado() {

    }
}
