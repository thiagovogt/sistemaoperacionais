package com.sistemas;

/**
 * Created by matheusprusch on 5/12/15.
 */
public interface GameBoard {

    public void riscar(int x, int y);
    public String mostrarMatriz();
    public void fechaQuadrado();

}
