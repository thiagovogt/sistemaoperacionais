// Arquivo: PontinhosClient.java (Roland Teodorowitsch; 24 abr. 2015)
package pontinhosrmi;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.Scanner;

public class PontinhosClient {

    public static void main(String[] args) {
        try {
            System.out.println("---JOGO DO PONTINHO---  \nSistemas distribuidos - T1 \nProfessor: Roland Teodorowitsch");
            Scanner s = new Scanner(System.in);
            String username = "";
            String ip_ou_dns = "";
            String nome = "";
            int id = 0;
            System.out.println("Digite o endereço ip do servidor:");
            ip_ou_dns = s.nextLine();
            PontinhosInterface pontinhos = (PontinhosInterface) Naming.lookup("//" + ip_ou_dns + "/Pontinhos");
            int nomeInvalido = 0;
            while (nomeInvalido == 0) {
                System.out.println("Digite seu nome:");
                nome = s.nextLine();
                if (nome.equals("")) {
                    System.out.println("Nome inválido");
                    nomeInvalido = 0;
                } else {
                    username = nome;
                    id = pontinhos.registraJogador(username);
                    if (id == -1) {
                        System.out.println("Usuário já cadastrado");
                    } else if (id == -2) {
                        System.out.println("Numero máximo de jogadores");
                    } else {
                        nomeInvalido = 1;
                    }
                }
            }
            int mantemVivo = 1;
            int inicio = 0;
            System.out.println("Seu caracter no jogo é " + pontinhos.obtemCaracter(id));
            while (mantemVivo == 1) {
                if (pontinhos.temPartida(id) == 0) {
                    System.out.println("Aguardando adversário...");
                    while (pontinhos.temPartida(id) == 0);
                    System.out.println("Partida iniciada, seu oponente é: " + pontinhos.obtemOponente(id));
                    inicio = 1;
                } else if (pontinhos.ehMinhaVez(id) == 0) {
                    if (inicio == 0) {
                        System.out.println("Partida iniciada, seu oponente é: " + pontinhos.obtemOponente(id));
                        inicio = 1;
                    }
                    System.out.println("Aguardando jogado do oponente...");
                    while (pontinhos.ehMinhaVez(id) == 0);
                    System.out.println("fimdejogo: " + pontinhos.fimDeJogo(id));
                    if (pontinhos.fimDeJogo(id) == true) {
                        mantemVivo = 0;
                    }
                } else if (pontinhos.ehMinhaVez(id) == 1) {
                    System.out.println("GRADE:");
                    System.out.println("" + pontinhos.obtemGrade(id));
                    System.out.println("SCORE: " + pontinhos.nomeJogador(id) + ": "
                            + pontinhos.obtemPontos(id) + " VS "
                            + pontinhos.obtemOponente(id) + ": "
                            + pontinhos.obtemPontosAdversario(id));
                    Scanner entrada = new Scanner(System.in);
                    int rep = 0;
                    int x = 0, y = 0;
                    while (rep == 0) {
                        System.out.println("enviaJogada: Digite sua coordenada X: ");
                        x = entrada.nextInt();
                        System.out.printf("enviaJogada: Digite a coordenada Y: ");
                        y = entrada.nextInt();
                        if (pontinhos.enviaJogada(id, x, y) == 1) {//valida coordenada
                            System.out.println("enviou");
                            rep = 1;
                        } else {
                            System.out.println("Coordenada invalida, tente novamente...");
                        }
                    }
                    //pontinhos.enviaJogada(id, x, y);
                    System.out.println("GRADE - após jogada: \n" + pontinhos.obtemGrade(id));
                    System.out.println("SCORE: " + pontinhos.nomeJogador(id) + ": "
                            + pontinhos.obtemPontos(id) + " VS "
                            + pontinhos.obtemOponente(id) + ": "
                            + pontinhos.obtemPontosAdversario(id));
                }
                if (pontinhos.fimDeJogo(id) == true) {
                    mantemVivo = 0;
                }
            }
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println("");
            System.out.println(" ----  GRADE FINAL  ----");
            System.out.println("" + pontinhos.obtemGrade(id));
            System.out.println("SCORE: " + pontinhos.nomeJogador(id) + ": "
                    + pontinhos.obtemPontos(id) + " VS "
                    + pontinhos.obtemOponente(id) + ": "
                    + pontinhos.obtemPontosAdversario(id));
            if (pontinhos.obtemPontos(id) > pontinhos.obtemPontosAdversario(id)) {
                System.out.println("Final de jogo");
                System.out.println("Vencedor: " + pontinhos.nomeJogador(id) + " Pontos: " + pontinhos.obtemPontos(id));
                System.out.println("Perdedor: " + pontinhos.obtemOponente(id) + " Pontos: " + pontinhos.obtemPontosAdversario(id));
            } else if (pontinhos.obtemPontos(id) < pontinhos.obtemPontosAdversario(id)) {
                System.out.println("Final de jogo");
                System.out.println("Vencedor: " + pontinhos.obtemOponente(id) + " Pontos: " + pontinhos.obtemPontosAdversario(id));
                System.out.println("Perdedor: " + pontinhos.nomeJogador(id) + " Pontos: " + pontinhos.obtemPontos(id));
            } else if (pontinhos.obtemPontos(id) == pontinhos.obtemPontosAdversario(id)) {
                System.out.println("Final de jogo");
                System.out.println("Empate: " + pontinhos.nomeJogador(id) + " Pontos: " + pontinhos.obtemPontos(id));
                System.out.println("Empate: " + pontinhos.obtemOponente(id) + " Pontos: " + pontinhos.obtemPontosAdversario(id));
            }
        } catch (Exception e) {
            System.out.println("PontinhosClient failed.");
            e.printStackTrace();
        }
    }
}
