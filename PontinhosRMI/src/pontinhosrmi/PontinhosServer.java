package pontinhosrmi;
import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.RemoteException;

public class PontinhosServer {

	public static void main (String[] args) {
		try {
			java.rmi.registry.LocateRegistry.createRegistry(1099);
			System.out.println("RMI registry ready.");			
		} catch (RemoteException e) {
			System.out.println("RMI registry already running.");			
		}
		try {
			Naming.rebind ("Pontinhos", new Pontinhos ());
			System.out.println ("PontinhosServer is ready.");
		} catch (RemoteException | MalformedURLException e) {
			System.out.println ("PontinhosServer failed:");
		}
	}
	
}

