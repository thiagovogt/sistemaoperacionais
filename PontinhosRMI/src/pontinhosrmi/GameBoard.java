package pontinhosrmi;

/**
 * Created by matheusprusch on 5/12/15.
 */
public interface GameBoard {

    public int riscar(int x, int y);
    public String mostrarMatriz();
    public void fechaQuadrado(int linha, int coluna, char j);
}
