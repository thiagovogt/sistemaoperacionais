/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pontinhosrmi;

/**
 *
 * @author Thiago
 */
public class Jogador {
    private String nome;
    private int ID;
    private char carac;
    private boolean emJogo;
    private int ehMinhavez;//,0(nao),1(sim),2(o vencedor),3(o perdedor),4(houve empate)
    private int idGrade;
    private int pontos;

      public Jogador(String nome, int ID) {
        this.nome = nome;
        this.ID = ID;
        ehMinhavez = -1;
    }

    public String getNome() {
        return nome;
    }

    public int getID() {
        return ID;
    }

    public char getCarac() {
        return carac;
    }

    public void setCarac(char carac) {
        this.carac = carac;
    }
    
    public boolean isEmJogo() {
        return emJogo;
    }

    public void setEmJogo(boolean emJogo) {
        this.emJogo = emJogo;
    }

    public int getEhMinhavez() {
        return ehMinhavez;
    }

    public void setEhMinhavez(int ehMinhavez) {
        this.ehMinhavez = ehMinhavez;
    }

    public int getIdGrade() {
        return idGrade;
    }

    public void setIdGrade(int idGrade) {
        this.idGrade = idGrade;
    }

    public int getPontos() {
        return pontos;
    }

    public void setPontos(int pontos) {
        this.pontos = pontos;
    }
    
    
    
}
