// Arquivo: Pontinhos.java (Roland Teodorowitsch; 24 abr. 2015)
package pontinhosrmi;

import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;
import java.util.ArrayList;
import java.util.Scanner;

public class Pontinhos extends UnicastRemoteObject implements PontinhosInterface {

    private static final long serialVersionUID = -5132894732984L;
    private int controleId;
    private ArrayList<GameBoardImpl> jogos;
    private ArrayList<Jogador> jogadores;
    //ArrayList<String> bandas = new ArrayList<String> ();

    public Pontinhos() throws RemoteException {
        controleId = 0;
        jogadores = new ArrayList<Jogador>(100);
        jogos = new ArrayList<GameBoardImpl>(50);
    }
    /* 1) registraJogador
     Recebe: string com o nome do usuário/jogador
     Retorna: id (valor inteiro) do usuário (que corresponde a um número de identificação único para este
     usuário durante uma partida), ­1 se este usuário já está cadastrado ou ­2 se o número máximo de
     jogadores tiver sido atingido

     */

    public int registraJogador(String nome) throws RemoteException {
        if (jogadores.size() <= 99) {// valida se numero maximo de jogadores ja foi atingido
            System.out.println("registraJogador: Exitem vagas");
            for (Jogador j : jogadores) {//valida se existe o usuário ja é cadastrado
                if (j.getNome().equals(nome)) {
                    System.out.println("registraJogador: Usuário já cadastrado");
                    return -1;
                }
            }
            Jogador j = new Jogador(nome, controleId);
            jogadores.add(j);
            System.out.println("registraJogador: numero de jogadores " + jogadores.size());
            System.out.println("registraJogador: Registro de jogador: " + nome + " ID: " + controleId);
            if (j.getID() % 2 == 0) {//se jogador for id par vai riscar como A, se impar vai riscar como B;
                j.setCarac('A');
                System.out.println("registraJogador: Jogador A");
            } else {
                j.setCarac('B');
                System.out.println("registraJogador: Jogador B");
                criaGrade(controleId);
            }
            return controleId++;
        } else {
        }
        return -2;
    }

    /* 2) temPartida
     Recebe: id do usuario(obtido atraves da chamada registraJogador)
     Retorna: -1(erro), 0(ainda nao ha partida), 1(sim, ha partida e o jogador inicia jogando,
     identificado com o caractere A)ou 2 (sim, ha partida e o jogador o segundo a jogar, identificado
     com o caractere B )
     */
    @Override
    public int temPartida(int IDjogador) {
        if (IDjogador >= 0 || IDjogador < 100) {
            Jogador aux = buscaJogador(IDjogador);
            if (aux.isEmJogo() == false) {// ainda não ha partida
                //System.out.println("temPartida: ainda não ha partida");
                return 0;
            } else if (aux.getCarac() == 'A') {//sim, ha partida e o jogador inicia jogando,identificado com o caractere A
                //System.out.println("temPartida: caractere A");
                return 1;
            } else if (aux.getCarac() == 'B') {//sim, ha partida e o jogador o segundo a jogar, identificado com o caractere B 
                //System.out.println("temPartida: caractere B");
                return 2;
            }
        }
        System.out.println("temPartida: erro");
        return -1;//erro
    }

    /* 3) ehMinhaVez
     Recebe: id do usuario (obtido atraves da chamada registra Jogador)
     Retorna:-1(erro),0(nao),1(sim),2(o vencedor),3(o perdedor),4(houve empate)
     Observacao:se ainda nao houver 2 jogadores registrados na partida, esta chamada retorna o codigo
     de erro -1
     */
    @Override
    public int ehMinhaVez(int IDjogador) {
        Jogador aux = buscaJogador(IDjogador);
        if (!aux.equals(null)) {
            return aux.getEhMinhavez();
        }
        return -1;
    }

    /* 4) obtemGrade
     Recebe: id do usuario (obtido atraves da chamada registraJogador)
     Retorna:string vazio em caso de erro ou string com a grade de jogo
     */
    @Override
    public String obtemGrade(int IDjogador) {
        return jogos.get(buscaJogador(IDjogador).getIdGrade()).mostrarMatriz();
    }

    /* 5) enviaJogada
     Recebe: id do usuario (obtido atraves da chamada registraJogador)), a posicaoo da aresta a ser
     posicionada�(veja a se o exemplo de Jogo para uma sugestao de especificacao de arestas)
     Retorna:1(tudo certo), 0 (movimento invalido) ou -1(erro)
     */
    @Override
    public int enviaJogada(int IDjogador, int x, int y) {
        if (IDjogador >= 0 && IDjogador < 100) {
            Jogador aux = buscaJogador(IDjogador);
            System.out.println("enviaJogada: Jogador " + aux.getNome() + " envie sua jogada");
            if(jogos.get(aux.getIdGrade()).riscar(x, y)==0)return 0;
            int pts = jogos.get(aux.getIdGrade()).verificaFechaQuadrado(x, y, aux.getCarac());
            if (pts > 0) {
                aux.setPontos(aux.getPontos() + pts);
            }
            aux.setEhMinhavez(0);//mda pra 0 para informar qu não mais a vez
            for (Jogador j : jogadores) {//passa a vez para o adversário
                if (j.getIdGrade() == aux.getIdGrade() && j.getCarac() != aux.getCarac()) {
                    //System.out.println("mudou par o adversário");
                    j.setEhMinhavez(1);
                }
            }
            return 1;
        }
        return -1;
    }

    /* 6) obtemOponente
     Recebe: id do usuario (obtido atraves da chamada registraJogador)
     Retorna:string vazio para erro ou string com o nome do oponente
     */
    @Override
    public String obtemOponente(int IDjogador) {
        Jogador aux = buscaJogador(IDjogador);
        for (Jogador j : jogadores) {
            if (j.getIdGrade() == aux.getIdGrade()) {
                if (j.getID() != aux.getID()) {
                    return j.getNome();
                }
            }
        }
        return "";
    }

    /* 7) obtemPontos
     Recebe: id do usuario (obtido atraves da chamada registraJogador)
     Retorna: erro(-1)ou numero de pontos do jogador
     */
    @Override
    public int obtemPontos(int IDjogador) {
        Jogador aux = buscaJogador(IDjogador);
        return aux.getPontos();
    }

    /*8) obtemPontosAdvers�rio
     Recebe: id do usuario (obtido atraves da chamada registraJogador)
     Retorna: erro(-1)ou numero de pontos do adversario
     */
    @Override
    public int obtemPontosAdversario(int IDjogador) {
        Jogador aux = buscaJogador(IDjogador);
        for (Jogador j : jogadores) {
            if (j.getIdGrade() == aux.getIdGrade()) {
                if (j.getID() != aux.getID()) {
                    return j.getPontos();
                }
            }
        }
        return -1;
    }

    /* 9) encerraJogo
     Recebe: id do usuario (obtido atraves da chamada registraJogador)
     Retorna:codigo de sucesso(0 indica sucesso e -1, erro)
     */
    @Override
    public int encerraJogo(int IDjogador) throws RemoteException {
        Jogador aux = buscaJogador(IDjogador);
        return 0;
    }

    /*
     10)nome do jogador
     recebeoid do jogador
     retorna nome do respectivo id
     */
    @Override
    public String nomeJogador(int IDjogador) throws RemoteException {
        Jogador aux = buscaJogador(IDjogador);
        return aux.getNome();
    }

    /*
     11)obtem caracter
     recebe id do jogador
     retorna o caracter de jogo
     */
    @Override
    public char obtemCaracter(int IDjogador) throws RemoteException{
        Jogador aux = buscaJogador(IDjogador);
        return aux.getCarac();
    }
    
       /*
    12) final de jogo
    recebe id do jogador
    returna true se jogo acabou, false caso jogo continue
    */
    @Override
    public boolean fimDeJogo(int IDjogador)throws RemoteException{
        Jogador j = buscaJogador(IDjogador);
        return jogos.get(j.getIdGrade()).finalDeJogo();
    }
    
    /*
     recebe idJogador
     retorna jogador
     */
    private Jogador buscaJogador(int IDjogador) {
        for (Jogador j : jogadores) {
            if (j.getID() == IDjogador) {
                //  System.out.println("buscaJogador: Retornou o jogador");
                return j;
            }
        }
        //System.out.println("buscaJogador: Não encontrou o jogador");
        return null;
    }

    /*
     recebe idJogador
     retorna grade de jogo
     */
    private GameBoardImpl buscaGrade(int IDjogador) {
        Jogador aux = buscaJogador(IDjogador);
        return jogos.get(aux.getIdGrade());
    }

    /*cria grade de jogo
     recebe controlerID, para saber qualjogador estar no tabuleiros
     retorna id do tabuleiro
     */
    private int criaGrade(int controleID) {
        System.out.println("criarGrade: inicio");
        System.out.println("criarGrade: controleID " + controleID);
        GameBoardImpl jogo = new GameBoardImpl(2);//parametro>1
        jogos.add(jogo);
        System.out.println("CriarGrade: id " + (jogos.size() - 1));
        jogadores.get(controleID - 1).setIdGrade(jogos.size() - 1);
        jogadores.get(controleID - 1).setEmJogo(true);
        jogadores.get(controleID - 1).setEhMinhavez(1);
        jogadores.get(controleID).setIdGrade(jogos.size() - 1);
        jogadores.get(controleID).setEmJogo(true);
        jogadores.get(controleID).setEhMinhavez(0);
        return jogos.size() - 1;
    }

}
