// Arquivo: PontinhosClient.java (Roland Teodorowitsch; 24 abr. 2015)
import java.rmi.Naming;

public class PontinhosClient {
	public static void main(String[] args) {
		String ip_ou_dns = "localhost";
		String username = "teste";

		try {
			PontinhosInterface pontinhos = (PontinhosInterface) Naming.lookup ("//"+ip_ou_dns+"/Pontinhos");
			int id = pontinhos.registraJogador(username);
			System.out.println (username+"->"+id);
		} catch (Exception e) {
			System.out.println ("PontinhosClient failed.");
			e.printStackTrace();
		}
	}
}

