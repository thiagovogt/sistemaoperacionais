// Arquivo: PontinhosInterface.java (Roland Teodorowitsch; 24 abr. 2015)
import java.rmi.Remote;
import java.rmi.RemoteException;

public interface PontinhosInterface extends Remote {
    /* 1) registraJogador
        Recebe:�string�com�o�nome�do�usu�rio/jogador
        Retorna:�id�(valor�inteiro)�do�usu�rio�(que�corresponde�a�um�n�mero�de�identifica��o��nico�para�este
        usu�rio�durante�uma�partida),��1�se�este�usu�rio�j�est�cadastrado�ou��2�se�o�n�mero�m�ximo�de
        jogadores�tiver�sido�atingido
    */
    public int registraJogador (String nome) throws RemoteException;
    
    /* 2) temPartida
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna: � �1 � (erro), � 0 � (ainda � n�o � h� � partida), � 1 � (sim, � h� � partida � e � o � jogador � inicia � jogando,
        identificado�com�o�caractere��A�)�ou�2�(sim,�h�partida�e�o�jogador��o�segundo�a�jogar,�identificado
        com�o�caractere��B�)
    */
    public int temPartida(int IDjogadr)throws RemoteException;
    
    /* 3) ehMinhaVez
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:��1�(erro),�0�(n�o),�1�(sim),�2�(�o�vencedor),�3�(�o�perdedor),�4�(houve�empate)
        Observa��o:�se�ainda�n�o�houver�2�jogadores�registrados�na�partida,�esta�chamada�retorna�o�c�digo
        de�erro��1
    */
    public int ehMinhaVez(int IDjogador)throws RemoteException;
    
    /* 4) obtemGrade
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:�string�vazio�em�caso�de�erro�ou�string�com�a�grade�de�jogo
    */
    public String obtemGrade(int IDjogador)throws RemoteException;
    
    /* 5) enviaJogada
        Recebe: � id � do � usu�rio � (obtido � atrav�s � da � chamada � registraJogador), � posi��o � da � aresta � a � ser
        posicionada�(veja�a�se��o�Exemplo�de�Jogo�para�uma�sugest�o�de�especifica��o�de�arestas)
        Retorna:�1�(tudo�certo),�0�(movimento�inv�lido)�ou��1�(erro)
    */
    public int enviaJogada(int IDjogador)throws RemoteException;
    
    /* 6) obtemOponente
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:�string�vazio�para�erro�ou�string�com�o�nome�do�oponente
    */
    public String obtemOponente(int IDjogador)throws RemoteException;
    
    /* 7) obtemPontos
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:�erro�(�1)�ou�n�mero�de�pontos�do�jogador
    */
    public int obtemPontos(int IDjogador)throws RemoteException;
    
    /*8) obtemPontosAdvers�rio
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:�erro�(�1)�ou�n�mero�de�pontos�do�advers�rio
    */
    public int obtemPontosAdvers�rio(int IDjogador)throws RemoteException;
    
    /* 9) encerraJogo
        Recebe:�id�do�usu�rio�(obtido�atrav�s�da�chamada�registraJogador)
        Retorna:�c�digo�de�sucesso�(0�indica�sucesso�e��1,�erro)
    */
    public int encerraJogo(int IDjogador)throws RemoteException;    
}

