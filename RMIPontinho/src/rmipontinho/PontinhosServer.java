// Arquivo: PontinhosServer.java (Roland Teodorowitsch; 24 abr. 2015)
import java.rmi.Naming;
import java.rmi.RemoteException;

public class PontinhosServer {

	public static void main (String[] args) {
		try {
			java.rmi.registry.LocateRegistry.createRegistry(1099);
			System.out.println("RMI registry ready.");			
		} catch (RemoteException e) {
			System.out.println("RMI registry already running.");			
		}
		try {
			Naming.rebind ("Pontinhos", new Pontinhos ());
			System.out.println ("PontinhosServer is ready.");
		} catch (Exception e) {
			System.out.println ("PontinhosServer failed:");
			e.printStackTrace();
		}
	}
	
}

