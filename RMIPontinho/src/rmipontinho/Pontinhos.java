// Arquivo: Pontinhos.java (Roland Teodorowitsch; 24 abr. 2015)
import java.rmi.RemoteException;
import java.rmi.server.UnicastRemoteObject;

public class Pontinhos extends UnicastRemoteObject implements PontinhosInterface{

	private static final long serialVersionUID = -5132894732984L;

	public Pontinhos () throws RemoteException {
	}

	public int registraJogador(String nome) throws RemoteException {
		System.out.println("Registro de jogador: "+nome);
		return 1234;
	}

}

